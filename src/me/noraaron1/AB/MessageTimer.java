package me.noraaron1.AB;

import org.bukkit.Bukkit;

public class MessageTimer implements Runnable{
	
	private MainAB plugin;
	public MessageTimer(MainAB instance){
		plugin = instance;
	}

	@Override
	public void run() {
		if(plugin.fc.getList("Message") != null && plugin.fc.getBoolean("MultiLine")){
			for(int i = 0; i < plugin.fc.getList("Message").size(); i++){		
				Bukkit.broadcastMessage(plugin.fc.getList("Message").get(i) + "");
			}
			return;
		}
		Bukkit.broadcastMessage(plugin.fc.getList("Message").get(0) + "");
	}

}
