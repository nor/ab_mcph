package me.noraaron1.AB;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public class MainAB extends JavaPlugin implements Listener{
	
	 File file = new File(getDataFolder() + "/config.yml");
	FileConfiguration fc = YamlConfiguration.loadConfiguration(file);
	final String message = fc.getString("Message");
	final Boolean bool = fc.getBoolean("MultiLine");
	int i2 = fc.getInt("Long");
	int a;
	ArrayList<String> messages = new ArrayList<String>();
	
	@Override
	public void onEnable() {
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(!fc.contains("Long")){
			fc.set("Long", "5");
		}if(!fc.contains("Message")){
			fc.set("Message", "Your Message Here");
		}
		if(!fc.contains("MultiLine")){
			fc.set("MultiLine", "false");
		}
		save();
		runnable(i2);
		Bukkit.getPluginManager().registerEvents(this, this);
	}
	
	@Override
	public void onDisable() {
		// TODO Auto-generated method stub
		super.onDisable();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("autobroadcaster")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				if(!p.hasPermission("autobroadcaster.use")){
					p.sendMessage(ChatColor.RED + "You don't have the correct permission to use this command!");
					return true;
				}
			}
			String prefix = ChatColor.GOLD + "[" + ChatColor.BLUE + "AutoBroadcaster" + ChatColor.GOLD + "]" + ChatColor.GREEN;
			if(args.length == 0){
				sender.sendMessage(ChatColor.AQUA + "=== " + prefix + ChatColor.AQUA + " ===");
				sender.sendMessage(prefix + "Not enough args! Correct args are either: setLong , setMessage, setMultiLineBoolean");
				sender.sendMessage(prefix + "Current message: ");
				sender.sendMessage(prefix + ChatColor.RESET + ((fc.getList("Message") == null) 
						? fc.getList("Message").get(0) : ((fc.getBoolean("MultiLine")) ? sendLists(prefix, sender) : fc.getList("Message").get(0))));
				sender.sendMessage(prefix + "Current time between messages: " + ChatColor.RESET + fc.getInt("Long"));
			}else if(args.length > 0){
				if(args[0].equalsIgnoreCase("setMessage")){
					if(args.length > 1){
						
						String newString = "";
				            for (int i = 1; i < args.length; i++) {
				                newString = newString.concat(args[i] + " ");
				            }
						fc.set("Message", newString);
						save();
						sender.sendMessage(prefix + "set message to: " + newString);
					}else{
						sender.sendMessage(prefix + "Not enough args for setMessage! You must supply a message.");
					}
				}else if(args[0].equalsIgnoreCase("setLong")){
					if(args.length == 2){
						try{
							int i = Integer.parseInt(args[1]);
							fc.set("Long", i);
							save();
							sender.sendMessage(prefix + "Set Long to: " + i);
							Bukkit.getScheduler().cancelTask(a);
							i2 = i;
							runnable(i);
						}catch(NumberFormatException e){
							sender.sendMessage(prefix + "Your input is not a number!");
							return true;
						}
					}else{
						sender.sendMessage(prefix + "Not enough args for setLong! You must supply an Integer.");
					}
				}else if(args[0].equalsIgnoreCase("setMultiLineBoolean")){
					if(args.length == 2){
							Boolean boo = Boolean.parseBoolean(args[1]);
							fc.set("MultiLine", boo);
							save();
							sender.sendMessage(prefix + "Set MultiLine to: " + boo);
					}else{
						sender.sendMessage(prefix + "Not enough args for setLong! You must supply an Boolean!");
					}
				}else if(args[0].equalsIgnoreCase("startEditer")){
					if(fc.getBoolean("MultiLine")){
						if(sender instanceof Player){
							Player p = (Player)sender;
							p.setMetadata("abMultiLine", new FixedMetadataValue(this, p.getMetadata("abMultiLine")));
							p.sendMessage(prefix + "You can now set a multi line message by just talking! To end the editer, type: /autobroadcaster endEditer");
							p.sendMessage(prefix + ChatColor.RED + "WARNING: BE CAREFUL WITH WHAT YOU TYPE! WHATEVER YOU TYPE WILL BE IN THE MESSAGE!");
						}else{
							sender.sendMessage(prefix + ChatColor.RED + " You can only set the MultiLines ingame!");							
						}
					}else{
						sender.sendMessage(prefix + "The multi line function is not enabled! To enable it, type: /autobroadcaster setMultiLineBoolean");
					}
				}else if(args[0].equalsIgnoreCase("endEditer")){
					if(fc.getBoolean("MultiLine")){
						if(sender instanceof Player){
							Player p = (Player)sender;
							fc.set("Message", Arrays.asList(messages));
							save();
							p.removeMetadata("abMultiLine", this);
							p.sendMessage(prefix + "You are now not the editer. You can chat normally!");
							messages.removeAll(messages);
						}else{
							sender.sendMessage(prefix + ChatColor.RED + " You can only set the MultiLines ingame!");							
						}
					}else{
						sender.sendMessage(prefix + "The multi line function is not enabled! To enable it, type: /autobroadcaster setMultiLineBoolean");
					}
				}else{
					sender.sendMessage(prefix + "Unknown command!");
				}
			}
		}
		return true;
	}
	
	@EventHandler
	public void onPlayerAsyncChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		if(p.hasMetadata("abMultiLine")){
			messages.add(e.getMessage());
			p.sendMessage(e.getMessage());
			e.setCancelled(true);
		}
	}
	
	private void save(){
		try {
			fc.save(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void runnable(int i){
		BukkitTask task = Bukkit.getScheduler().runTaskTimer(this, new MessageTimer(this) ,20*60*i, 20*60*i); //20*60*time
		a = task.getTaskId();
	}
	
	private String sendLists(String prefix, CommandSender sender){
		sender.sendMessage(prefix + "");
		for(int i = 0; i < fc.getList("Message").size(); i++){		
			sender.sendMessage(prefix + "Line " + (i+1) + ": " + ChatColor.RESET + fc.getList("Message").get(i));
		}
		return "";
	}
	
//	private String replaced(String message){
//		message = message.replace("", "" + ChatColor.BLACK);
//		message = message.replace("", "" + ChatColor.DARK_BLUE);
//		message = message.replace("", "" + ChatColor.DARK_GREEN);
//		message = message.replace("", "" + ChatColor.);
//		message.replace("", "" + ChatColor.);
//		message.replace("", "" + ChatColor.);
//		message.replace("", "" + ChatColor.);
//		message.replace("", "" + ChatColor.);
//		return message;
//	}
}
